#!/usr/bin/env python

import rospy
import tf.transformations
import numpy as np
import tf2_ros
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Pose, PoseStamped
from interactive_markers.interactive_marker_server import (
    InteractiveMarkerServer,
    InteractiveMarkerFeedback,
)
from visualization_msgs.msg import InteractiveMarker, InteractiveMarkerControl, Marker
from geometry_msgs.msg import PoseStamped
import pinocchio as pin
import time

marker_pose = PoseStamped()
pose_pub = None
# [[min_x, max_x], [min_y, max_y], [min_z, max_z]]
position_limits = [[-0.6, 0.6], [-0.6, 0.6], [0.05, 0.9]]


def makeBox(msg):
    marker = Marker()

    # marker.type = Marker.SPHERE
    marker.type = Marker.CUBE
    marker.scale.x = msg.scale * 0.45
    marker.scale.y = msg.scale * 0.45
    marker.scale.z = msg.scale * 0.45
    marker.color.r = 0.5
    marker.color.g = 0.5
    marker.color.b = 0.5
    marker.color.a = 0.3

    return marker

def publisher_callback(msg, link_name):
    marker_pose.header.frame_id = link_name
    marker_pose.header.stamp = rospy.Time(0)
    pose_pub.publish(marker_pose)

def process_feedback(feedback):
    if feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
        marker_pose.pose.position.x = max(
            [
                min([feedback.pose.position.x, position_limits[0][1]]),
                position_limits[0][0],
            ]
        )
        marker_pose.pose.position.y = max(
            [
                min([feedback.pose.position.y, position_limits[1][1]]),
                position_limits[1][0],
            ]
        )
        marker_pose.pose.position.z = max(
            [
                min([feedback.pose.position.z, position_limits[2][1]]),
                position_limits[2][0],
            ]
        )
        marker_pose.pose.orientation = feedback.pose.orientation
    server.applyChanges()

def SE3ToPosemsg(SE3):
    msg = Pose()
    p = SE3.translation
    msg.position.x, msg.position.y, msg.position.z = p[0], p[1], p[2]
    q = pin.Quaternion(SE3.rotation)
    msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w = (
        q.x,
        q.y,
        q.z,
        q.w,
    )
    return msg

def wait_for_initial_pose():
   
    joints = rospy.wait_for_message(
        state_topic, JointState, timeout=30
    )

    q = joints.position
    robot_description = rospy.get_param(robot_description_param_name)
    model = pin.buildModelFromXML(robot_description)
    q = np.hstack([q, np.zeros(model.nv - len(q))])
    data = model.createData()
    
    frame_id = model.nframes - 1
    pin.forwardKinematics(model, data, q=np.array(q))
    current_pose = pin.updateFramePlacement(model, data, frame_id)

    msg = SE3ToPosemsg(current_pose)
    marker_pose.pose.orientation = msg.orientation
    marker_pose.pose.position = msg.position

if __name__ == "__main__":
    rospy.init_node("interactive_marker")
    time.sleep(5)
    state_topic = rospy.get_param('interactive_marker/state_topic') 
    robot_description_param_name = rospy.get_param('interactive_marker/robot_description_param_name') 
    # listener = tf.TransformListener()
    link_name = "world"
    tfBuffer = tf2_ros.Buffer()
    _ = tf2_ros.TransformListener(tfBuffer)

    wait_for_initial_pose()

    pose_pub = rospy.Publisher("interactive_pose", PoseStamped, queue_size=10)
    server = InteractiveMarkerServer("interactive_marker")
    int_marker = InteractiveMarker()
    int_marker.header.frame_id = link_name
    int_marker.scale = 0.3
    int_marker.name = "interactive_pose"
    int_marker.pose = marker_pose.pose
    # run pose publisher
    rospy.Timer(rospy.Duration(0.005), lambda msg: publisher_callback(msg, link_name))

    # insert a box
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 1
    control.orientation.y = 0
    control.orientation.z = 0
    control.name = "rotate_x"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)

    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 1
    control.orientation.y = 0
    control.orientation.z = 0
    control.name = "move_x"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 1
    control.orientation.z = 0
    control.name = "rotate_y"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 1
    control.orientation.z = 0
    control.name = "move_y"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 0
    control.orientation.z = 1
    control.name = "rotate_z"
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 0
    control.orientation.z = 1
    control.name = "move_z"
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)

    control = InteractiveMarkerControl()
    control.always_visible = True
    control.interaction_mode = InteractiveMarkerControl.MOVE_3D
    int_marker.controls.append(control)
    control.markers.append(makeBox(int_marker))

    server.insert(int_marker, process_feedback)

    server.applyChanges()

    rospy.spin()
