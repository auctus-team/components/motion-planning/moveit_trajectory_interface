#pragma once

#include "pinocchio/fwd.hpp"
#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/multibody/model.hpp"
#include "pinocchio/algorithm/model.hpp"

#include <atomic>
#include <ros/node_handle.h>
#include <ros/ros.h>

#include <moveit_trajectory_interface/Follow.h>
// MoveIt
#include <actionlib/server/simple_action_server.h>
#include <control_msgs/JointTrajectoryAction.h>
#include <eigen_conversions/eigen_msg.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <moveit_trajectory_interface/waypoint_interpolation.hpp>
#include <realtime_tools/realtime_publisher.h>
#include <sensor_msgs/JointState.h>
#include <std_srvs/Trigger.h>
#include <visualization_msgs/InteractiveMarkerFeedback.h>

#include <chrono>

class MoveItTrajectory
{
  public:
    MoveItTrajectory() : follow_flag{false}, trajectory_is_built{false} {};
    void
    init(ros::NodeHandle node_handle, std::string robot_description_param_name,
         Eigen::VectorXd q_init,
         std::vector<std::string> joint_names = std::vector<std::string>());
    bool toggle_follow_flag(moveit_trajectory_interface::Follow::Request &req,
                            moveit_trajectory_interface::Follow::Response &res);

    void stop();
    bool load_robot_model(std::string robot_description, std::vector<std::string> joint_names = std::vector<std::string>());

    void setControlledFrame(std::string controlled_frame);
    /**
     * \brief
     * Get the desired Pose
     */
    Eigen::Affine3d getCartesianPose();

    /**
     * \brief
     * Get the desired Twist
     */
    Eigen::Matrix<double,6,1> getCartesianVelocity();

    /**
     * \brief
     * Get the desired Acceleration
     */
    Eigen::Matrix<double,6,1> getCartesianAcceleration();

    Eigen::VectorXd getJointAcceleration();

    Eigen::VectorXd getJointVelocity();
    
    Eigen::VectorXd getJointConfiguration();
    void initializeJointConfiguration(Eigen::VectorXd q);
    ros::NodeHandle getNodeHandle();
    bool computeNewTrajectory(trajectory_msgs::JointTrajectory path);
    void updateTrajectory();
    void publish_trajectory();
    double getTimeProgress();
    void setTrajectoryTimeIncrement(double time_increment);
    bool isInitialized();
    bool isDone();
    std::string getRobotName();
    void sortJointState(trajectory_msgs::JointTrajectory& msg);
    bool isTrajectoryBuild();
    void setMaxVelocityScalingFactor(double scaling_factor);
    void setMaxAccelerationScalingFactor(double scaling_factor);
    void follow_target_pose(const visualization_msgs::InteractiveMarkerFeedback &feedback);
    bool pauseTrajectory(std_srvs::Trigger::Request &req,
                         std_srvs::Trigger::Response &res);

  private:
    template<typename T>
    bool is_in_vector(const std::vector<T> & vector, const T & elt)
    {
      return vector.end() != std::find(vector.begin(),vector.end(),elt);
    }

    ros::NodeHandle node_handle;
    // Model information
    pinocchio::Model model; /*!< @brief pinocchio model*/ 
    pinocchio::Data data; /*!< @brief pinocchio data*/
    std::string controlled_frame;
    bool is_done = false;
    //Trajectory parameters
    double t_traj = 0.0;
    Eigen::Affine3d cartesian_pose;
    pinocchio::SE3 follow_pose;
    std::atomic<bool> follow_flag;

    Eigen::Matrix<double,6,1> cartesian_velocity;
    Eigen::Matrix<double,6,1> cartesian_acceleration;
    Eigen::VectorXd joint_configuration ;
    Eigen::VectorXd joint_velocity ;
    Eigen::VectorXd joint_acceleration ;
    std::atomic<bool> trajectory_is_built;

    // ROS Publishers
    realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_des_publisher;
    realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped> vel_des_publisher,acc_des_publisher;
    realtime_tools::RealtimePublisher<sensor_msgs::JointState> joint_state_des_publisher;
    geometry_msgs::Twist cartesian_velocity_msg,cartesian_acceleration_msg;
    geometry_msgs::Pose cartesian_pose_msg;
    sensor_msgs::JointState joint_state_des_msg;
    double time_increment = 0.001;
    bool control_frame_set = false;
    bool is_initialized = false;
    std::string planning_group;
    std::string robot_description_param_name;
    std::unique_ptr<WaypointInterpolation> waypoint_interpolation;
    trajectory_msgs::JointTrajectory path;
    double number_of_waypoint;
    double waypoint_index = 0;
    bool joints_are_unordered = false;
    double waypoints_duration;
    double total_time = 0.0;
    bool is_paused = false;
    ros::ServiceServer pause_service;
    ros::Subscriber follow_subscriber;
    ros::ServiceServer follow_service;
};
