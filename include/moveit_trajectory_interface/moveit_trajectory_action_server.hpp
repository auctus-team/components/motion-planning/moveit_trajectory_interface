#pragma once

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <control_msgs/JointTrajectoryAction.h>
#include <moveit_trajectory_interface/moveit_trajectory.hpp>

class TrajectoryAction
{
protected:

  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<control_msgs::JointTrajectoryAction> as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  std::string action_name_;
  // create messages that are used to published feedback/result
  control_msgs::JointTrajectoryFeedback feedback_;
  control_msgs::JointTrajectoryResult result_;
  std::shared_ptr<MoveItTrajectory> trajectory;

public:

  TrajectoryAction(std::string name, std::shared_ptr<MoveItTrajectory> trajectory ) :
    as_(nh_, name, boost::bind(&TrajectoryAction::executeCB, this, _1), false),
    action_name_(name)
  {
    this->trajectory = trajectory;
    as_.start();
  }

  ~TrajectoryAction(void)
  {
  }

  void executeCB(const control_msgs::JointTrajectoryGoalConstPtr &goal)
  {
    ros::Rate r(1);
    bool success = true;
    success = trajectory->computeNewTrajectory(goal->trajectory);

    while (trajectory->getTimeProgress() <1.0)
    {
      // check that preempt (meaning cancelled) has not been requested by the client
      if (as_.isPreemptRequested() || !ros::ok())
      {
          ROS_ERROR("%s: Preempted", action_name_.c_str());
          // set the action state to preempted
          as_.setPreempted();
          trajectory->stop();
          success = false;
      }
      // feedback_.time_ratio = trajectory->getTimeProgress() ;
      // publish the feedback
      as_.publishFeedback(feedback_);
    }

    if(success)
    {
      as_.setSucceeded(result_);
    }
  }
};
