#pragma once
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <Eigen/Dense>
#include <trajectory_interface/quintic_spline_segment.h>
// #include <trajectory_interface/pos_vel_acc_state.h>
class WaypointInterpolation{
    public:
    void initWaypoint(trajectory_msgs::JointTrajectoryPoint start,trajectory_msgs::JointTrajectoryPoint end);
    void updateWaypoint(double time);
    bool waypointReached();
    Eigen::VectorXd getPosition();
    Eigen::VectorXd getVelocity();
    Eigen::VectorXd getAcceleration();
    trajectory_interface::PosVelAccState<double> interpolated_state;
    std::unique_ptr<trajectory_interface::QuinticSplineSegment<double>> interpolation;
    double waypoint_time = 0.0;
    double start_time = 0.0;
    double time;
    bool waypoint_reached;
};