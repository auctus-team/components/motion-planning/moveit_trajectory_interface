#include <moveit_trajectory_interface/waypoint_interpolation.hpp>

void WaypointInterpolation::initWaypoint(trajectory_msgs::JointTrajectoryPoint start,trajectory_msgs::JointTrajectoryPoint end)
{
    int size = start.positions.size();
    trajectory_interface::PosVelAccState<double> start_state(size);
    trajectory_interface::PosVelAccState<double> end_state(size);
    
    for (int i =0 ; i<size ; ++i)
    {
        start_state.position[i] = start.positions[i];
        start_state.velocity[i] = start.velocities[i];
        start_state.acceleration[i] = start.accelerations[i];

        end_state.position[i] = end.positions[i];
        end_state.velocity[i] = end.velocities[i];
        end_state.acceleration[i] = end.accelerations[i];
        
    }
    time=0.0;
    start_time = start.time_from_start.toSec();
    waypoint_time = end.time_from_start.toSec() - start.time_from_start.toSec();
    waypoint_reached = false;
    // std::make_unique interpolation(0, start_state, end.time_from_start.toSec() - start.time_from_start.toSec(), end_state);
    interpolation.reset(new trajectory_interface::QuinticSplineSegment<double>(0, start_state, waypoint_time , end_state));
}

void WaypointInterpolation::updateWaypoint(double time)
{
    time -= start_time ;
    if (time > waypoint_time)
    {
        time = waypoint_time;
        waypoint_reached = true;
    }
    interpolation->sample(time,interpolated_state);
}

bool WaypointInterpolation::waypointReached()
{
    return waypoint_reached;
}


Eigen::VectorXd WaypointInterpolation::getPosition()
{
    return Eigen::VectorXd::Map(interpolated_state.position.data(),interpolated_state.position.size());
}

Eigen::VectorXd WaypointInterpolation::getVelocity()
{
    return Eigen::VectorXd::Map(interpolated_state.velocity.data(),interpolated_state.velocity.size());
}

Eigen::VectorXd WaypointInterpolation::getAcceleration()
{
    return Eigen::VectorXd::Map(interpolated_state.acceleration.data(),interpolated_state.acceleration.size());
}
