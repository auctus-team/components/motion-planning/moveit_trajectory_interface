#include "moveit_trajectory_interface/moveit_trajectory_interface.hpp"

void TrajectoryInterface::action_thread(std::shared_ptr<MoveItTrajectory> interface)
{
    while(!interface->isInitialized())
    {
        ROS_DEBUG_STREAM_THROTTLE(1,"Wait for trajectory manager to be initialized");
    }
    TrajectoryAction trajectory_action(interface->getNodeHandle().getNamespace() +  "/moveit_trajectory_interface/trajectory_action", interface);
    ros::MultiThreadedSpinner spinner(2);
    while(ros::ok())
    {
         spinner.spin();
    }
}