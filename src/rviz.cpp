/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2017, PickNik Consulting
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of PickNik Consulting nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Dave Coleman
   Desc:   Rviz display panel for controlling and debugging ROS applications
*/

#include <cstdio>
#include <cstdlib>

#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QSpinBox>
#include <QTimer>

#include <QIcon>
#include <QStyle>

#include <ros/node_handle.h>

#include <ros/master.h>

#include <QApplication>
#include <QStyle>
#include <QToolButton>

#include "rviz.hpp"
#include <cmath>
#include <ios>
#include <sstream>

namespace rviz_robot_plugins {

InteractiveMarker::InteractiveMarker(QWidget *parent) : rviz::Panel(parent) {
  // ros services
  push_button_client =
      nh.serviceClient<moveit_trajectory_interface::Follow>("/moveit_trajectory_interface_follow");

  auto *layout = new QVBoxLayout;
  layout->setAlignment(Qt::AlignHCenter);

  {
    topic_combo = new QComboBox;
    auto *hlayout = new QHBoxLayout;
    hlayout->setAlignment(Qt::AlignHCenter);
    hlayout->addWidget(topic_combo);
    QToolButton *refresh_button = new QToolButton;
    refresh_button->setIcon(
        QApplication::style()->standardIcon(QStyle::SP_BrowserReload));

    auto update_topics = [=]() {
      ros::master::V_TopicInfo topic_infos;
      ros::master::getTopics(topic_infos);
      QStringList pose_topics;
      // std::cout<<"Pose topics found:"<<std::endl;
      const std::string Pose = "InteractiveMarkerFeedback";
      for (auto const &topic : topic_infos) {
        if (topic.datatype.find(Pose) != std::string::npos) {
          pose_topics << QString::fromStdString(topic.name);
          std::cout << topic.name << "->" << topic.datatype << std::endl;
        }
      }

      std::string default_topic = "";
      const std::string Marker = "marker";
      for (auto const &topic : pose_topics) {
        if (topic.toStdString().find(Marker) != std::string::npos) {
          default_topic = topic.toStdString();
          break;
        }
      }


      topic_combo->clear();
      topic_combo->addItems(pose_topics);
      if (!default_topic.empty()) {
        topic_combo->setCurrentText(QString::fromStdString(default_topic));
      }
    };

    update_topics();
    connect(refresh_button, &QToolButton::clicked, [=]() { update_topics(); });

    hlayout->addWidget(refresh_button);
    layout->addLayout(hlayout);
  }

  push_button = new QPushButton(this);
  push_button->setText("Follow Marker");
  auto *hlayout = new QHBoxLayout;
  hlayout->setAlignment(Qt::AlignHCenter);
  hlayout->addWidget(push_button);
  {
    QLabel *tmpLabel = new QLabel("OFF");
    connect(push_button, &QPushButton::clicked, this, [=]() {
      const std::string ON = "ON";
      const std::string OFF = "OFF";
      if (tmpLabel->text().toStdString() == ON) {
        tmpLabel->setText(QString::fromStdString(OFF));
        follow_msg.request.flag = false;
      } else {
        tmpLabel->setText(QString::fromStdString(ON));
        follow_msg.request.flag = true;
      }
      follow_msg.request.topic = topic_combo->currentText().toStdString();
      push_button_client.call(follow_msg);
    });
    hlayout->addWidget(tmpLabel);
  }
  layout->addLayout(hlayout);

  const int big_line_length = 10;
  const int small_line_length = 5;
  // ------------------------------------------------
  // slider
  {
    QFrame *line = new QFrame(this);
    line->setFrameShape(QFrame::HLine); // Horizontal line
    line->setFrameShadow(QFrame::Sunken);
    line->setLineWidth(small_line_length);
    layout->addWidget(line);
  }
  // ------------------------------------------------------
  {
    QPushButton *push_button2 = new QPushButton(this);
    push_button2->setText("Recover Robot");
    auto *hlayout2 = new QHBoxLayout;
    hlayout2->setAlignment(Qt::AlignHCenter);
    hlayout2->addWidget(push_button2);
    connect(push_button2, &QPushButton::clicked, this, [=]() {
      std::string command =
          "rostopic pub -1 /panda/franka_control/error_recovery/goal "
          "franka_msgs/ErrorRecoveryActionGoal {}";
      std::cout << command << std::endl;
      std::cout << system(command.c_str()) << std::endl;
    });
    layout->addLayout(hlayout2);
  }
  // Vertical layout
  setLayout(layout);
}

void InteractiveMarker::save(rviz::Config config) const { rviz::Panel::save(config); }

void InteractiveMarker::load(const rviz::Config &config) { rviz::Panel::load(config); }
} // namespace rviz_robot_plugins

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(rviz_robot_plugins::InteractiveMarker, rviz::Panel)
