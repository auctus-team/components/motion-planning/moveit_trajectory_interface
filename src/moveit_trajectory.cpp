/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <moveit_trajectory_interface/moveit_trajectory.hpp>


void MoveItTrajectory::init( ros::NodeHandle node_handle, std::string robot_description_param_name,Eigen::VectorXd q_init, std::vector<std::string> joint_names)
{
  this->robot_description_param_name = robot_description_param_name;
  this->node_handle = node_handle;
  
  std::string robot_description;
  node_handle.getParam(robot_description_param_name,robot_description);
  load_robot_model(robot_description,joint_names);

  pinocchio::forwardKinematics(model,data,q_init);
  pinocchio::updateFramePlacements(model,data);

  if (!control_frame_set)
    controlled_frame = model.frames[model.nframes-1].name;
  cartesian_pose.matrix() = data.oMf[model.getFrameId(controlled_frame)].toHomogeneousMatrix();

  follow_pose = data.oMf[model.getFrameId(controlled_frame)];

  cartesian_velocity = Eigen::MatrixXd::Zero(6,1);
  cartesian_acceleration = Eigen::MatrixXd::Zero(6,1);

  initializeJointConfiguration(q_init);
  joint_state_des_msg.name = joint_names;
  joint_state_des_msg.position.resize(model.nv);
  joint_state_des_msg.velocity.resize(model.nv);
  joint_state_des_msg.effort.resize(model.nv);
  pose_des_publisher.init(node_handle, "desired_cartesian_pose", 1);
  vel_des_publisher.init(node_handle, "desired_cartesian_velocity", 1);
  acc_des_publisher.init(node_handle, "desired_cartesian_acceleration", 1);
  joint_state_des_publisher.init(node_handle,"desired_joint_state",1);
  waypoint_interpolation.reset(new WaypointInterpolation);

  // SERVICES
  follow_service =
      node_handle.advertiseService("/moveit_trajectory_interface_follow",
                                   &MoveItTrajectory::toggle_follow_flag, this);
  pause_service =
      node_handle.advertiseService("moveit_trajectory_interface/pause",
                                   &MoveItTrajectory::pauseTrajectory, this);

  is_initialized = true;
}

bool MoveItTrajectory::pauseTrajectory(std_srvs::Trigger::Request &req,
                                       std_srvs::Trigger::Response &res) {
  is_paused = !is_paused;
  if (is_paused) {
    ROS_WARN_STREAM("Trajectory paused");
  } else {
    ROS_WARN_STREAM("Trajectory resumed");
  }
  return true;
}

void MoveItTrajectory::initializeJointConfiguration(Eigen::VectorXd q) {
  joint_configuration = q;
  joint_velocity = Eigen::VectorXd::Zero(model.nv);
  joint_acceleration = Eigen::VectorXd::Zero(model.nv);
}

bool MoveItTrajectory::toggle_follow_flag(
    moveit_trajectory_interface::Follow::Request &req,
    moveit_trajectory_interface::Follow::Response &res) {
  if (req.flag == false) {
    trajectory_is_built.store(false);
    follow_flag.store(false);
    ROS_DEBUG_STREAM("turn OFF topic following");
    follow_subscriber.shutdown();
  } else {
    // TODO: it should point to the current pose when turning on
    // target_pose = current_pose;
    follow_flag.store(true);
    trajectory_is_built.store(false); // TODO should it turn be false?
    ROS_DEBUG_STREAM("turn ON topic following");

    follow_subscriber = getNodeHandle().subscribe(
        req.topic, 1000, &MoveItTrajectory::follow_target_pose, this);
  }
  return true;
}

void MoveItTrajectory::follow_target_pose(
    const visualization_msgs::InteractiveMarkerFeedback &feedback) {

  // if (follow_update_delay.passed()) {
    Eigen::Isometry3d eig;
    tf::poseMsgToEigen(feedback.pose, eig);
    // target_pose = pinocchio::SE3(eig.rotation(), eig.translation());
    follow_pose = pinocchio::SE3(eig.matrix());
    if (follow_flag.load()) {
        cartesian_pose.matrix() = follow_pose.toHomogeneousMatrix();
    }
    cartesian_velocity = Eigen::MatrixXd::Zero(6, 1);
    cartesian_acceleration = Eigen::MatrixXd::Zero(6, 1);
    // follow_update_delay.refresh();
  // }
}

ros::NodeHandle MoveItTrajectory::getNodeHandle()
{
  return node_handle;
}


bool MoveItTrajectory::isInitialized()
{
  return is_initialized;
}

std::string MoveItTrajectory::getRobotName()
{
  return model.name;
}

bool MoveItTrajectory::load_robot_model(std::string robot_description, std::vector<std::string> joint_names)
{
    pinocchio::Model temp_model;
    pinocchio::urdf::buildModelFromXML(robot_description,temp_model,false);
    if (!joint_names.empty())
    {
        std::vector<pinocchio::JointIndex> list_of_joints_to_keep_unlocked_by_id;
        for(std::vector<std::string>::const_iterator it = joint_names.begin();
            it != joint_names.end(); ++it)
        {
            const std::string & joint_name = *it;
            if(temp_model.existJointName(joint_name))
              list_of_joints_to_keep_unlocked_by_id.push_back(temp_model.getJointId(joint_name));

        }
        
        // Transform the list into a list of joints to lock
        std::vector<pinocchio::JointIndex> list_of_joints_to_lock_by_id;
        for(pinocchio::JointIndex joint_id = 1; joint_id < temp_model.joints.size(); ++joint_id)
        {
            const std::string joint_name = temp_model.names[joint_id];
            if(is_in_vector(joint_names,joint_name))
            continue;
            else
            {
            list_of_joints_to_lock_by_id.push_back(joint_id);
            }
        }
        
        // Build the reduced temp_model from the list of lock joints
        Eigen::VectorXd q_rand = randomConfiguration(temp_model);
        model = pinocchio::buildReducedModel(temp_model,list_of_joints_to_lock_by_id,q_rand);
    }
    data = pinocchio::Data(model);

    return true;
}

void MoveItTrajectory::setTrajectoryTimeIncrement(double time_increment)
{
  this->time_increment = time_increment;
}

bool MoveItTrajectory::isTrajectoryBuild()
{
  return trajectory_is_built.load();
}

void MoveItTrajectory::updateTrajectory()
{
  if (trajectory_is_built.load() and !is_paused) {
    total_time += time_increment; 
    waypoint_interpolation->updateWaypoint(total_time);
    joint_configuration = waypoint_interpolation->getPosition();
    joint_velocity = waypoint_interpolation->getVelocity();
    joint_acceleration = waypoint_interpolation->getAcceleration();
    pinocchio::forwardKinematics(model,data,joint_configuration , joint_velocity,joint_acceleration);
    pinocchio::updateFramePlacements(model,data);
    cartesian_pose.matrix() = data.oMf[model.getFrameId(controlled_frame)].toHomogeneousMatrix();
  
    pinocchio::Motion xd = getFrameVelocity(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::LOCAL);
    pinocchio::Motion ad = getFrameAcceleration(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::LOCAL);
      
    cartesian_velocity = xd.toVector();
    cartesian_acceleration = ad.toVector();

    if (waypoint_interpolation->waypointReached())
    {
      waypoint_index ++;
      if (waypoint_index < number_of_waypoint-1)
      {
        waypoint_interpolation->initWaypoint(path.points[waypoint_index],path.points[waypoint_index+1]);
        // std::cout << "going from \n" << path.points[waypoint_index] << "\n to \n" << path.points[waypoint_index+1] << std::endl; 
      }
      else
      {
        is_done = true;
        trajectory_is_built.store(false);
        total_time = path.points[number_of_waypoint-1].time_from_start.toSec();
      }
      
    }
  }
  publish_trajectory();
}

bool MoveItTrajectory::isDone()
{
  return is_done;
}

Eigen::Affine3d MoveItTrajectory::getCartesianPose()
{
  return cartesian_pose;
}

Eigen::Matrix<double,6,1> MoveItTrajectory::getCartesianVelocity()
{
  return cartesian_velocity;
}

Eigen::Matrix<double,6,1> MoveItTrajectory::getCartesianAcceleration()
{
  return cartesian_acceleration;
}

Eigen::VectorXd MoveItTrajectory::getJointConfiguration()
{
  return joint_configuration;
}

Eigen::VectorXd MoveItTrajectory::getJointVelocity()
{
  return joint_velocity;
}

Eigen::VectorXd MoveItTrajectory::getJointAcceleration()
{
  return joint_acceleration;
}

void MoveItTrajectory::setControlledFrame(std::string controlled_frame)
{
  this->controlled_frame = controlled_frame;
  control_frame_set = true;
}


void MoveItTrajectory::sortJointState(trajectory_msgs::JointTrajectory& msg)
{
    // This function is used to sort the joint comming from the /joint_state topic
    // They are order alphabetically while we need them to be order as in the URDF file
    // With use the joint_names variable that defined the joints in the correct order
    trajectory_msgs::JointTrajectory sorted_joint_state = msg;
    std::vector<std::string> joint_names_from_urdf;
    for(pinocchio::JointIndex joint_id = 1; joint_id < (pinocchio::JointIndex)model.njoints; ++joint_id)
    {
      joint_names_from_urdf.push_back(model.names[joint_id]);
    }
    if (joint_names_from_urdf != msg.joint_names)
    {
      for ( int num_points = 0 ; num_points< msg.points.size(); num_points ++)
      {
        int i = 0;
        for (auto ith_unsorted_joint_name: msg.joint_names)
        {
            int j=0;
            for (auto jth_joint_name : joint_names_from_urdf)    
            {
                if(ith_unsorted_joint_name.find(jth_joint_name) != std::string::npos)
                {
                    sorted_joint_state.points[num_points].positions[j] = msg.points[num_points].positions[i];
                    sorted_joint_state.joint_names[j] = msg.joint_names[i];
                    break;
                }
                else
                {
                    j++;
                }
            }
            i++;
        }
      }
    }
    msg =  sorted_joint_state;
}

bool MoveItTrajectory::computeNewTrajectory(trajectory_msgs::JointTrajectory path)
{
  // if following marker, disable moveit
  if (follow_flag.load()) {
    return true;
  }

  this->path = path;
  number_of_waypoint = this->path.points.size() ;
  if (number_of_waypoint < 2)
  {
    ROS_WARN( "Empty trajectory not doing anything");
    total_time = this->path.points[number_of_waypoint-1].time_from_start.toSec();
  }
  else
  {
    sortJointState(this->path);

    waypoint_index = 0;
    for (int i=0 ; i<model.nv; i++)
    {
      this->path.points[waypoint_index].velocities[i] = 0.0;
      this->path.points[waypoint_index].accelerations[i] = 0.0;
      this->path.points[number_of_waypoint-1].velocities[i] = 0.0;
      this->path.points[number_of_waypoint-1].accelerations[i] = 0.0;
    }
    waypoint_interpolation->initWaypoint(this->path.points[waypoint_index],this->path.points[waypoint_index+1]);
    total_time = 0.0;
    trajectory_is_built.store(true);
    is_done = false;
  }
  is_paused = false;
  return true;
}

void MoveItTrajectory::publish_trajectory()
{
  // Publishing in LOCAL_WORLD_ALIGN reference frame for a better readability
  pinocchio::Motion xd = getFrameVelocity(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED);
  pinocchio::Motion ad = getFrameAcceleration(model, data, model.getFrameId(controlled_frame),pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED);
      
  tf::poseEigenToMsg(cartesian_pose,cartesian_pose_msg);
  tf::twistEigenToMsg(xd.toVector(), cartesian_velocity_msg);
  tf::twistEigenToMsg(ad.toVector(), cartesian_acceleration_msg);
  
  //Publish robot desired pose
  if (pose_des_publisher.trylock())
  {
      pose_des_publisher.msg_.header.stamp = ros::Time::now();
      pose_des_publisher.msg_.header.frame_id = "world";
      pose_des_publisher.msg_.pose = cartesian_pose_msg;
      pose_des_publisher.unlockAndPublish();
  }
  //Publish robot desired velocity
  if (vel_des_publisher.trylock())
  {
      vel_des_publisher.msg_.header.stamp = ros::Time::now();
      vel_des_publisher.msg_.header.frame_id = "world";
      vel_des_publisher.msg_.twist = cartesian_velocity_msg;
      vel_des_publisher.unlockAndPublish();
  }
  //Publish robot desired acceleration

  if (acc_des_publisher.trylock())
  {
      acc_des_publisher.msg_.header.stamp = ros::Time::now();
      acc_des_publisher.msg_.header.frame_id = "world";
      acc_des_publisher.msg_.twist = cartesian_acceleration_msg;
      acc_des_publisher.unlockAndPublish();
  }
  for (int i = 0; i < model.nv ; ++i)
  {
    joint_state_des_msg.position[i] =  getJointConfiguration()[i];
    joint_state_des_msg.velocity[i] = getJointVelocity()[i];
    joint_state_des_msg.effort[i] = getJointAcceleration()[i];
  }
  if (joint_state_des_publisher.trylock())
  {
    joint_state_des_publisher.msg_.header.stamp = ros::Time::now();
    joint_state_des_publisher.msg_.header.frame_id = "world";
    joint_state_des_publisher.msg_ = joint_state_des_msg;
    joint_state_des_publisher.unlockAndPublish();
  }
}

void MoveItTrajectory::stop()
{
  trajectory_is_built.store(false);
  cartesian_velocity = Eigen::MatrixXd::Zero(6,1);
  cartesian_acceleration = Eigen::MatrixXd::Zero(6,1);
  total_time = path.points[number_of_waypoint - 1].time_from_start.toSec();
}

double MoveItTrajectory::getTimeProgress()
{
  return total_time / path.points[number_of_waypoint-1].time_from_start.toSec();
}

